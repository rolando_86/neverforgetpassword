package com.project.neverforgetpassword.service;

import com.project.neverforgetpassword.entity.SystemCredentials;
import com.project.neverforgetpassword.exception.NoFoundException;
import com.project.neverforgetpassword.repository.SystemCredentialsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SystemCredentialsService {

    private final SystemCredentialsRepository systemCredentialsRepository;

    @Autowired
    public SystemCredentialsService(SystemCredentialsRepository systemCredentialsRepository) {
        this.systemCredentialsRepository = systemCredentialsRepository;
    }

    public List<SystemCredentials> getAllSystemCredentials() {
        return systemCredentialsRepository.findAll();
    }

    public SystemCredentials saveSystemCredentials(SystemCredentials systemCredentials) {
        return systemCredentialsRepository.save(systemCredentials);
    }

    public SystemCredentials getSystemCredentials(Long id) throws NoFoundException {
        Optional<SystemCredentials> optional = systemCredentialsRepository.findById(id);
        if (!optional.isPresent()) {
            throw new NoFoundException("Not Found System Credentials");
        }
        return optional.get();
    }

    public void deleteSystemCredentials(Long id) throws NoFoundException {
        Optional<SystemCredentials> optional = systemCredentialsRepository.findById(id);
        if (!optional.isPresent()) {
            throw new NoFoundException("Not Found System Credentials");
        }
        systemCredentialsRepository.deleteById(id);
    }
}
