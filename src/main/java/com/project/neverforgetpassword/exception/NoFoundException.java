package com.project.neverforgetpassword.exception;

public class NoFoundException extends Exception {
    public NoFoundException(String message) {
        super(message);
    }
}
