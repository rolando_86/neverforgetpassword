package com.project.neverforgetpassword;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NeverForgetPasswordApplication {

	public static void main(String[] args) {
		SpringApplication.run(NeverForgetPasswordApplication.class, args);
	}

}
