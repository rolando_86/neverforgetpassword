package com.project.neverforgetpassword.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@NoArgsConstructor
@Entity
public class SystemCredentials {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;
    @Column
    private String systemName;
    @Column
    private String userName;
    @Column
    private String email;
    @Column
    private String password;
}
