package com.project.neverforgetpassword.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long dni;
    @Column
    private String nameLastName;
    @Column
    private String userNameNFP;
    @Column
    private String passwordNFP;

}
