package com.project.neverforgetpassword.repository;

import com.project.neverforgetpassword.entity.SystemCredentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SystemCredentialsRepository extends JpaRepository<SystemCredentials,Long> {
}
