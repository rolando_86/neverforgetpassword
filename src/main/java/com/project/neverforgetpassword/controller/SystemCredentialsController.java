package com.project.neverforgetpassword.controller;

import com.project.neverforgetpassword.entity.SystemCredentials;
import com.project.neverforgetpassword.exception.NoFoundException;
import com.project.neverforgetpassword.service.SystemCredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/systemcredentials")
public class SystemCredentialsController {

    private final SystemCredentialsService systemCredentialsService;

    @Autowired
    public SystemCredentialsController(SystemCredentialsService systemCredentialsService) {
        this.systemCredentialsService = systemCredentialsService;
    }

    @GetMapping
    public String getAllSystemCredentials(Model model) {
        List<SystemCredentials> systems = systemCredentialsService.getAllSystemCredentials();
        /*SystemCredentials aux = new SystemCredentials();
        aux.setId(1L);
        aux.setEmail("email");
        aux.setPassword("*****");
        aux.setUserName("username");
        aux.setSystemName("netflix");
        systems.add(aux);*/
        model.addAttribute("systems", systems);
        return "index";
    }

    @GetMapping("/add/{id}")
    public String showSaveSystemCredentials(@PathVariable("id") Long id, Model model) throws NoFoundException {
        if (id != null && id != 0) {
            model.addAttribute("systemCredentials", systemCredentialsService.getSystemCredentials(id));
        }else{
            model.addAttribute("systemCredentials", new SystemCredentials());
        }
        return "add";
    }

    @PostMapping("/add")
    public String saveSystemCredentials(SystemCredentials systemCredentials, Model model) {
        systemCredentialsService.saveSystemCredentials(systemCredentials);
        return "redirect:/systemcredentials";
    }

    @GetMapping("/delete/{id}")
    public String deleteSystemCredentials(@PathVariable("id") Long id, Model model) throws NoFoundException {
        systemCredentialsService.deleteSystemCredentials(id);
        return "redirect:/systemcredentials";
    }


}
